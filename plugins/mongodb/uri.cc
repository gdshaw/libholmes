// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "error.h"
#include "uri.h"

namespace holmes::db::mongodb {

uri::uri(const std::string& uri_string) {
	bson_error_t error;
	_uri = mongoc_uri_new_with_error(uri_string.c_str(), &error);
	if (!_uri) {
		throw mongodb::error(error);
	}
}

uri::~uri() {
	mongoc_uri_destroy(_uri);
}

std::string uri::dbname() const {
	const char* _dbname = mongoc_uri_get_database(_uri);
	return std::string(_dbname ? _dbname : "");
}

} /* namespace holmes::db::mongodb */
