// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "libbson_object.h"

namespace holmes::db::mongodb {

libbson_object::libbson_object() {
	_bson = bson_new();
}

libbson_object::libbson_object(const bson::object& that) {
	octet::string raw = that.encode();
	_bson = bson_new_from_data(raw.data(), raw.length());
}

} /* namespace holmes::db::mongodb */
