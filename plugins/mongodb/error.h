// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_DB_MONGODB_ERROR
#define HOLMES_DB_MONGODB_ERROR

#include <stdexcept>

#include <mongoc.h>

namespace holmes::db::mongodb {

/** A class to represent an error originating from MongoDB. */
class error:
	public std::runtime_error {
public:
	/** Construct MongoDB error from string.
	 * @param message the error message
	 */
	error(const std::string& message):
		runtime_error(message) {}

	/** Construct MongoDB error from bson_error_t.
	 * @param error a bson_error_t containing the error message
	 */
	error(const bson_error_t& error):
		runtime_error(error.message) {}
};

} /* namespace holmes::db::mongodb */

#endif
