// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_DB_MONGODB_LIBBSON_OBJECT
#define HOLMES_DB_MONGODB_LIBBSON_OBJECT

#include <mongoc.h>

#include "holmes/bson/object.h"

namespace holmes::db::mongodb {

/** An RAII class for managing a libbson bson_t. */
class libbson_object {
private:
	/** The underlying bson_t structure. */
	bson_t* _bson;
public:
	/** Construct empty bson_t. */
	libbson_object();

	libbson_object(const libbson_object&) = delete;

	/** Construct bson_t from bson::object.
	 * @param that the bson::object to copy
	 */
	libbson_object(const bson::object& that);

	/** Destroy bson_t structure. */
	~libbson_object() {
		bson_destroy(_bson);
	}

	libbson_object& operator=(const libbson_object&) = delete;

	/** Access the underlying bson_t.
	 * @return a pointer to the bson_t.
	 */
	operator bson_t*() {
		return _bson;
	}
};

} /* namespace holmes::db::mongodb */

#endif
