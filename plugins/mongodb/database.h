// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_DB_MONGODB_DATABASE
#define HOLMES_DB_MONGODB_DATABASE

#include <string>

#include <mongoc.h>

#include "holmes/db/database.h"
#include "uri.h"

namespace holmes::db::mongodb {

/** A database class to represent a MongoDB database. */
class database:
	public db::database {
private:
	/** The MongoDB URI. */
	mongodb::uri _uri;

	/** The database name. */
	std::string _dbname;

	/** The underlying mongoc client. */
	mongoc_client_t* _client;
public:
	/** Open connection to MongoDB database.
	 * @param uri the URI of the database
	 */
	database(const std::string& uri_string);

	virtual ~database();
	virtual std::unique_ptr<db::collection> collection(const std::string& name);
};

} /* namespace holmes::db::mongodb */

#endif
