// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_DB_MONGODB_COLLECTION
#define HOLMES_DB_MONGODB_COLLECTION

#include <string>

#include <mongoc.h>

#include "holmes/db/collection.h"

namespace holmes::db::mongodb {

/** A class to represent a MongoDB collection. */
class collection:
	public db::collection {
private:
	/** The underlying mongoc collection object. */
	mongoc_collection_t* _collection;
public:
	/** Construct MongoDB collection.
	 * @param client the mongoc client instance
	 * @param dbname the MongoDB database name
	 * @param clname the MongoDB collection name
	 */
	collection(mongoc_client_t& client,
		const std::string& dbname, const std::string& clname);

	virtual ~collection();
	virtual bson::array find(const bson::object& filter,
		unsigned int limit);
	virtual void update(const bson::object& before,
		const bson::object& after);
};

} /* namespace holmes::db::mongodb */

#endif
