// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_DB_MONGODB_URI
#define HOLMES_DB_MONGODB_URI

#include <string>

#include <mongoc.h>

namespace holmes::db::mongodb {

/** A class to represent a MongoDB URI. */
class uri {
private:
	/** The MongoDB uri. */
	mongoc_uri_t* _uri;
public:
	/** Construct MongoDB URI from string. */
	uri(const std::string& uri_string);

	/** Destroy MongoDB URI. */
	virtual ~uri();

	uri(const uri&) = delete;
	uri& operator=(const uri&) = delete;

	operator mongoc_uri_t*() const {
		return _uri;
	}

	/** Get database name.
	 * @return the database name, or the empty string if none
	 */
	std::string dbname() const;
};

} /* namespace holmes::db::mongodb */

#endif
