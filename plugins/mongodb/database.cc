// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <memory>

#include "error.h"
#include "collection.h"
#include "database.h"

namespace holmes::db::mongodb {

/** A class for initialising the mongoc library.
 * This class is included within the same compilation unit as
 * mongodb_database to ensure that it is linked into the executable
 * when necessary. It does not need to be publically accessible,
 * so need not appear in the header file.
 */
class mongoc_library {
private:
	/** The single instance of this class. */
	static const mongoc_library instance;

	/** Initialise library. */
	mongoc_library();

	/** Cleanup library. */
	~mongoc_library();
};

mongoc_library::mongoc_library() {
	mongoc_init();
}

mongoc_library::~mongoc_library() {
	mongoc_cleanup();
}

const mongoc_library mongoc_library::instance;

database::database(const std::string& uri_string):
	_uri(uri_string),
	_dbname(_uri.dbname()) {

	if (_dbname.empty()) {
		throw mongodb::error("MongoDB database name not specified");
	}
	_client = mongoc_client_new_from_uri(_uri);
}

database::~database() {
	mongoc_client_destroy(_client);
}

std::unique_ptr<db::collection> database::collection(
	const std::string& name) {

	return std::make_unique<db::mongodb::collection>(*_client, _dbname, name);
}

} /* namespace holmes::db::mongodb */

extern "C"
std::unique_ptr<holmes::db::database> make_database(
	const std::string& uri_string) {

	return std::make_unique<holmes::db::mongodb::database>(uri_string);
}
