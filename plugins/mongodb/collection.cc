// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cstdint>

#include "holmes/bson/boolean.h"
#include "holmes/bson/int32.h"

#include "libbson_object.h"
#include "collection.h"

namespace holmes::db::mongodb {

collection::collection(mongoc_client_t& client,
	const std::string& dbname, const std::string& clname):
	_collection(mongoc_client_get_collection(
		&client, dbname.c_str(), clname.c_str())) {}

collection::~collection() {
	if (_collection) {
		mongoc_collection_destroy(_collection);
	}
}

bson::array collection::find(const bson::object& filter,
	unsigned int limit) {

	libbson_object bson_opts;
	bson_append_int64(bson_opts, "limit", -1, limit);

	libbson_object bson_query(filter);

	bson::array results;
	mongoc_cursor_t* cursor = mongoc_collection_find_with_opts(
		_collection, bson_query, bson_opts, 0);
	const bson_t* bson_result = 0;
	bool found = mongoc_cursor_next(cursor, &bson_result);

	unsigned int count = 0;
	while (found && (count < limit)) {
		octet::string raw_result(bson_get_data(bson_result), bson_result->len);
		auto result = bson::object::decode(raw_result);
		results.append(*result);
		++count;
		found = mongoc_cursor_next(cursor, &bson_result);
	}

	mongoc_cursor_destroy(cursor);
	return results;
}

void collection::update(const bson::object& before,
	const bson::object& after) {

	bson::object filter;
	filter.insert("_id", before.at("_id"));
	try {
		filter["version"] = before.at("version");
	} catch (std::out_of_range&) {
		bson::object missing;
		missing["$exists"] = bson::boolean(false);
		filter["version"] = std::move(missing);
	}

	int32_t version = 0;
	try {
		version = before.at("version").as<bson::int32>().as_int32();
	} catch (std::out_of_range&) {
		/* no action */
	}
	bson::object _after(after);
	_after.insert("version", bson::int32(version + 1));

	octet::string raw_filter = filter.encode();
	bson_t bson_filter;
	bson_init_static(&bson_filter, raw_filter.data(), raw_filter.length());

	octet::string raw_after = _after.encode();
	bson_t bson_after;
	bson_init_static(&bson_after, raw_after.data(), raw_after.length());

	bool success = mongoc_collection_replace_one(
		_collection, &bson_filter, &bson_after, 0, 0, 0);
	if (!success) {
		throw std::runtime_error("failed to update document");
	}
}

} /* namespace holmes::db::mongodb */
