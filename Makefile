# This file is part of libholmes.
# Copyright 2020 Graham Shaw.
# Distribution and modification are permitted within the terms of the
# GNU General Public License (version 3 or any later version).

prefix = /usr/local
exec_prefix = $(prefix)
bindir = $(exec_prefix)/bin
libdir = $(exec_prefix)/lib
libexecdir = $(libdir)
mandir = $(prefix)/man
man1dir = $(mandir)/man1

pkgname = holmes

CPPFLAGS = -MD -MP -I. '-DLIBEXECDIR="$(libexecdir)"' '-DPKGNAME="$(pkgname)"'
CXXFLAGS = -fPIC -O2 --std=c++17 -Wall -Wpedantic
LDLIBS = -ldl

SRC = $(wildcard src/*.cc)
BIN = $(SRC:src/%.cc=bin/%)
MAINBIN = bin/$(pkgname)
AUXBIN = $(filter-out $(MAINBIN),$(BIN))

HOLMES = $(wildcard holmes/*.cc) $(wildcard holmes/*/*.cc) $(wildcard holmes/*/*/*.cc)

PLUGDIRS = $(wildcard plugins/*)
PLUGLIBS = $(foreach PLUGDIR,$(PLUGDIRS),$(PLUGDIR)/$(notdir $(PLUGDIR)).so)

MAN1 = $(wildcard man/man1/*.1)
MAN1GZ = $(MAN1:man/%=man/%.gz)
MANGZ = $(MAN1GZ)

.PHONY: all
all: $(BIN) $(PLUGLIBS) $(MANGZ)

$(BIN): bin/%: src/%.o holmes.so
	@mkdir -p bin
	g++ -rdynamic -Wl,-rpath $(libdir) -o $@ $^ $(LDLIBS)

plugins/%.so: always
	make -C $(dir $@)

holmes.so: $(HOLMES:%.cc=%.o)
	gcc -shared -o $@ $^

man/%.gz: man/%
	gzip -k -f $<

.PHONY: clean
clean: $(PLUGDIRS:%=%/clean)
	rm -f holmes/*.[do]
	rm -f holmes/*/*.[do]
	rm -f holmes/*/*/*.[do]
	rm -f src/*.[do]
	rm -f *.so
	rm -rf bin
	rm -f man/*/*.gz

%/clean: always
	make -C $* clean

.PHONY: install
install: all
	@mkdir -p $(bindir)
	@mkdir -p $(libdir)
	@mkdir -p $(libexecdir)/$(pkgname)/bin
	@mkdir -p $(libexecdir)/$(pkgname)/plugins
	cp $(MAINBIN) $(bindir)/
	cp holmes.so $(libdir)/
	cp $(AUXBIN) $(libexecdir)/$(pkgname)/bin/
	cp $(PLUGLIBS) $(libexecdir)/$(pkgname)/plugins/
	@mkdir -p $(man1dir)
	cp $(MAN1GZ) $(man1dir)/

.PHONY: uninstall
uninstall:
	rm -f $(bindir)/$(notdir $(MAINBIN))
	rm -rf $(libexecdir)/$(pkgname)
	rm -f $(foreach MANFILE,$(MAN1GZ),$(man1dir)/$(notdir $(MANFILE)))

.PHONY: always
always:

-include $(HOLMES:%.cc=%.d)
-include $(SRC:%.cc=%.d)
