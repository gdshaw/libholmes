// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include <dlfcn.h>

#include "holmes/db/database.h"

namespace holmes::db {

std::unique_ptr<database> database::make(const std::string& uri_string) {
	size_t index = uri_string.find(':');
	if (index == std::string::npos) {
		throw std::runtime_error("missing prefix in database URI " + uri_string);
	}
	std::string prefix = uri_string.substr(0, index);
	if (prefix.empty() || !isalpha(prefix[0])) {
		throw std::runtime_error("invalid prefix in database URI " + uri_string);
	}
	for (char c : prefix) {
		if (!isalnum(c) && (c != '+') && (c != '.') && (c != '-')) {
			throw std::runtime_error("invalid prefix in database URI " + uri_string);
		}
	}

	std::string pathname = std::string(
		LIBEXECDIR "/" PKGNAME "/plugins/") + prefix + ".so";
	void* dlh = dlopen(pathname.c_str(), RTLD_NOW);
	if (!dlh) {
		throw std::runtime_error(
			std::string("failed to load plugin: ") +
			dlerror());
	}

	typedef std::unique_ptr<database> database_maker(std::string);
	database_maker* maker = reinterpret_cast<database_maker*>(
		dlsym(dlh, "make_database"));
	if (!maker) {
		throw std::runtime_error(
			"make_database function missing from plugin");
	}
	return (*maker)(uri_string);
}

} /* namespace holmes::db */
