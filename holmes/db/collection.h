// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_DB_COLLECTION
#define HOLMES_DB_COLLECTION

#include "holmes/bson/value.h"
#include "holmes/bson/object.h"
#include "holmes/bson/array.h"

namespace holmes::db {

/** An abstract base class to represent a database collection. */
class collection {
public:
	/** Close collection. */
	virtual ~collection() = default;

	/** Find documents
	 * @param filter a filter for selecting the results
	 * @param limit the maximum number of docuemnts to find
	 */
	virtual bson::array find(const bson::object& filter,
		unsigned int limit) = 0;

	/** Update a document.
	 * @param before the value before the update
	 * @param after the required value after the update
	 */
	virtual void update(const bson::object& before,
		const bson::object& after) = 0;
};

} /* namespace holmes::db */

#endif
