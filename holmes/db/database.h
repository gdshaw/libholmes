// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_DB_DATABASE
#define HOLMES_DB_DATABASE

#include <memory>

#include "holmes/db/collection.h"

namespace holmes::db {

/** An abstract base class to represent a database. */
class database {
public:
	/** Close database. */
	virtual ~database() = default;

	/** Open collection within database.
	 * @param name the name of the collection to be opened
	 * @return the resulting collection
	 */
	virtual std::unique_ptr<db::collection> collection(
		const std::string& name) = 0;

	/** Open database from URI.
	 * @param uri_string the URI of the database to be opened
	 * @return the resulting database object, or 0 if none
	 */
	static std::unique_ptr<db::database> make(const std::string& uri_string);
};

} /* namespace holmes::db */

#endif
