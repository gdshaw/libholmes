// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_NET_TCP_SPLITTER
#define HOLMES_NET_TCP_SPLITTER

#include <memory>
#include <deque>

#include "holmes/net/tcp/connection.h"

namespace holmes::net::tcp {

/** A class for associating TCP segments with TCP connections.
 * The segments must be presented in the order they were observed, and must
 * relate to the same pair of endpoints (flowing in either direction).
 */
class splitter {
private:
	/** A list of active connections. */
	std::deque<std::unique_ptr<tcp::connection>> _connections;
public:
	/** Process a segment.
	 * @param segment the segment to be processed
	 * @return the connection which accepted the segment
	 */
	tcp::connection& process(const tcp::segment& segment);
};

} /* namespace holmes::net::tcp */

#endif
