// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "holmes/net/tcp/splitter.h"

namespace holmes::net::tcp {

tcp::connection& splitter::process(const tcp::segment& segment) {
	bool accepted = false;
	for (auto& connection : _connections) {
		accepted = connection->process(segment);
		if (accepted) {
			return *connection;
		}
	}

	auto connection = std::make_unique<tcp::connection>();
	_connections.push_front(std::move(connection));
	_connections.front()->process(segment);
	return *_connections.front();
}

} /* namespace holmes::net::tcp */
