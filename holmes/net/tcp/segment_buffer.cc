// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "holmes/octet/stream/listener.h"
#include "holmes/net/tcp/event.h"
#include "holmes/net/tcp/prefer_old_content.h"
#include "holmes/net/tcp/segment_buffer.h"

namespace holmes::net::tcp {

std::pair<segment_buffer::const_iterator, segment_buffer::const_iterator>
	segment_buffer::_equal_range(uint64_t seq) const {

	// Find the first entry which provides a sequence number greater than
	// the requested one. This will mark the end of the reported range.
	const_iterator last = _segments.upper_bound(seq);

	// If there is a preceding entry, and if that entry provides the
	// requested sequence number, then that entry is included in the
	// reported range. Otherwise, the reported range is empty (but still
	// ending at the point previously determined).
	auto first = last;
	if (first != _segments.begin()) {
		--first;
		if (!first->second.contains(seq)) {
			first = last;
		}
	}
	return std::pair<const_iterator, const_iterator>(first, last);
}

segment_buffer::segment_buffer(bool direction,
	std::list<octet::stream::listener*>* listeners,
	std::unique_ptr<reassembly_policy> policy):
	_direction(direction),
	_policy(policy ? std::move(policy) : std::make_unique<prefer_old_content>()),
	_listeners(listeners) {}

void segment_buffer::insert(const tcp::segment& new_seg) {
	uint64_t refseq = _curseq.value_or(0x80000000);
	uint64_t seq = new_seg.seq(refseq);
	uint64_t nextseq = new_seg.nextseq(refseq);
	bool using_new_seg = false;

	// Loop over the range of sequence numbers provided by the new segment.
	while (seq < nextseq) {
		// In the first instance, initialise the arguements that will be
		// passed to the reassembly policy to the values that would be
		// appropriate if the relevant part of the live segment list were
		// empty.
		uint64_t from_seq = seq;
		uint64_t to_seq = nextseq;
		const segment* prev_old_seg = 0;
		const segment* old_seg = 0;
		const segment* next_old_seg = 0;

		// Determine whether seq is already provided by an entry in the
		// live segment list.
		auto range = _equal_range(seq);
		if (range.first != range.second) {
			// If it is then update the policy arguments to refer to the
			// segment provided by that entry.
			old_seg = &range.first->second;

			if (range.first != _segments.begin()) {
				auto iter = range.first;
				prev_old_seg = &(--iter)->second;
			}
		}

		// Either way, the range for which a decision will be requested
		// is bounded by the next entry in the list, if there is one.
		// (Note that it is not generally bounded by the end of an existing
		// segment, because it would not be possible to replace that segment
		// without also filling any gap that follows it, assuming that the
		// new segment is long enough.)
		if (range.second != _segments.end()) {
			if (to_seq > range.second->first) {
				to_seq = range.second->first;
			}
			next_old_seg = &range.second->second;
		}

		// Decide whether the new or old segment is preferred.
		bool use_new_seg = _policy->choose(seq, _curseq.value_or(0),
			prev_old_seg, old_seg, next_old_seg, new_seg);

		if (use_new_seg) {
			// If the existing segment that is to be removed covers to_seq,
			// and if there is no table entry at to_seq, then create an entry
			// for the existing segment at to_seq.
			if (old_seg && (old_seg->nextseq(refseq) > to_seq)) {
				if (_segments.find(from_seq) != _segments.end()) {
					if (_segments.find(to_seq) == _segments.end()) {
						_segments[to_seq] = _segments[from_seq];
					}
				}
			}

			// The new segment is preferred.
			// Check whether the it has already been inserted into the list
			// as the previous entry (in which case there is no need for it
			// to appear a second time).
			if (using_new_seg) {
				// If already inserted then delete any entry at range.first.
				if (range.first != range.second) {
					_segments.erase(range.first);
				}
			} else {
				// Otherwise, insert it into the list, overwriting the
				// existing segment if there is one.
				_segments[from_seq] = new_seg;
				using_new_seg = true;
			}

			// Advance to the end of the range now covered by the new segment.
			seq = to_seq;
		} else if (old_seg) {
			// An existing segment was preferred.
			// Advance to the end of that existing segment (but do not advance
			// past any gap at the end of that segment, because a different
			// decision is possible and likely within the gap).
			using_new_seg = false;
			seq = old_seg->nextseq(refseq);
		} else {
			// The new segment was not used, even though there was no existing
			// segment. (This is unlikely to be a useful policy, but it is
			// handled here for completeness.)
			// Advance to the end of the gap.
			using_new_seg = false;
			seq = to_seq;
		}
	}

	if (new_seg.syn_flag() && !_curseq) {
		// SYN occupies a position in the sequence number space,
		// but should not be delivered as in-band payload.
		_curseq = new_seg.seq() + 1;
	}
}

void segment_buffer::_report(const event& ev) const {
	for (auto listener : *_listeners) {
		listener->handle(ev);
	}
}

void segment_buffer::deliver(uint64_t targetseq, bool allow_gaps) {
	if (!_curseq) {
		// The delivery process requires a value for curseq.
		// Normally this will have been set when the SYN segment was observed,
		// but if the SYN segment was missed then a value must be assumed.
		// The approach taken here is to report as a gap the single unit of
		// sequence space that is confidently known to be missing, but
		// not to speculate whether there was anything lost before that.
		// To achieve this, _curseq is set to either:
		// - one unit prior to the first segment in the list if there
		//   is such a segment, or
		// - one unit prior to targetseq if the list is empty.
		if (_segments.empty()) {
			_curseq = targetseq - 1;
		} else {
			_curseq = _segments.begin()->first - 1;
		}
	}

	while (*_curseq < targetseq) {
		// Search for the segment providing _curseq, if there is one.
		auto match = _equal_range(*_curseq);

		// Regardless of whether a matching entry was found, the next
		// reported event must not extend beyond:
		// - the target sequence number, or
		// - the next entry following _curseq if there is one.
		uint64_t limitseq = targetseq;
		if ((match.second != _segments.end()) && (match.second->first < limitseq)) {
			limitseq = match.second->first;
		}

		if (match.first != match.second) {
			// A matching entry was found.
			// This further limits the next reported event to not
			// extend beyond the end of the relevant segment.
			uint64_t nextseq = match.first->second.nextseq(*_curseq);
			if (nextseq < limitseq) {
				limitseq = nextseq;
			}
			_report(event(*_curseq, limitseq, &match.first->second, _direction));
			_curseq = limitseq;
		} else {
			if (!allow_gaps) {
				return;
			}
			// No entry found, therefore the data for curseq is missing
			// and should be reported as a gap.
			_report(event(*_curseq, limitseq, 0, _direction));
			_curseq = limitseq;
		}
	}
}

void segment_buffer::deliver(bool allow_gaps) {
	if (!_segments.empty()) {
		auto it = _segments.rbegin();
		uint64_t nextseq = it->second.nextseq(it->first);
		deliver(nextseq, allow_gaps);
	}
}

void segment_buffer::remove(uint64_t targetseq) {
	while (!_segments.empty()) {
		auto it = _segments.begin();
		uint64_t nextseq = it->second.nextseq(it->first);
		if (nextseq <= targetseq) {
			_segments.erase(it);
		} else {
			break;
		}
	}
}

} /* namespace holmes::net::tcp */
