// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "holmes/net/tcp/connection.h"

namespace holmes::net::tcp {

connection::connection():
	_client_server_buffer(true, &_listeners),
	_server_client_buffer(false, &_listeners) {}

bool connection::process(const tcp::segment& segment) {
	if (segment.syn_flag() && !segment.ack_flag()) {
		if (_initial_syn) {
			// Duplicate SYN segments should be recognised here,
			// but that is not yet supported.
			return false;
		}
		_initial_syn = segment;
	}

	// This heuristic is sufficient to enable testing,
	// but has obvious limitations.
	bool direction = segment.dst_port() < segment.src_port();

	if (direction) {
		_client_server_buffer.insert(segment);
		_client_server_buffer.deliver(false);
	} else {
		_server_client_buffer.insert(segment);
		_server_client_buffer.deliver(false);
	}
	return true;
}

void connection::flush() {
	_server_client_buffer.deliver(true);
	_client_server_buffer.deliver(true);
}

void connection::add_listener(octet::stream::listener& listener) {
	_listeners.push_back(&listener);
}

} /* namespace holmes::net::tcp */
