// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_NET_TCP_CONNECTION
#define HOLMES_NET_TCP_CONNECTION

#include <optional>
#include <list>

#include "holmes/octet/stream/listener.h"
#include "holmes/net/tcp/segment_buffer.h"

namespace holmes::net::tcp {

/** A class to represent a TCP connection. */
class connection {
private:
	/** A buffer for reassembling client to server traffic. */
	segment_buffer _client_server_buffer;

	/** A buffer for reassembling client to server traffic. */
	segment_buffer _server_client_buffer;

	/** The initial SYN segment, if one has been observed. */
	std::optional<segment> _initial_syn;

	/** A list of registered event listeners. */
	std::list<octet::stream::listener*> _listeners;

public:
	/** Construct an object to represent a new TCP connection. */
	connection();

	/** Process a segment.
	 * @param segment the segment to be processed
	 * @return true if the segment was accepted, otherwise false
	 */
	bool process(const tcp::segment& segment);

	/** Deliver any remaining buffered content. */
	void flush();

	/** Register an event listener to receive reassembled content.
	 * @param listener the event listener to be registered
	 */
	void add_listener(octet::stream::listener& listener);
};

} /* namespace holmes::net::tcp */

#endif
