// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_NET_TCP_SEGMENT_BUFFER
#define HOLMES_NET_TCP_SEGMENT_BUFFER

#include <cstdint>
#include <list>
#include <map>
#include <optional>

#include "holmes/octet/stream/listener.h"
#include "holmes/net/tcp/reassembly_policy.h"
#include "holmes/net/tcp/segment.h"

namespace holmes::net::tcp {

class event;

/** A class for buffering TCP segments travelling in the same direction. */
class segment_buffer {
private:
	/** The type of the live segment list. */
	typedef std::map<uint64_t, segment> segment_list;

	/** The type of a const iterator into the live segment list. */
	typedef segment_list::const_iterator const_iterator;

	/** The live segment list.
	 * A list of live segments belonging to this buffer, indexed by the first
	 * extended sequence number for which it is intended to provide content
	 * for the reassembly.
	 *
	 * The index must specify an extended sequence number that the segment is
	 * capable of providing.
	 *
	 * If the index is greater than the first extended sequence number that
	 * the segment is capable of providing, then a corresponding number of
	 * octets at the start of the segment shall be skipped.
	 *
	 * If a second segment is indexed to begin at or before the last extended
	 * sequence number that the first segment is capable of providing, then a
	 * corresponding number of octets at the end of the segment shall be
	 * skipped. Regardless of the length of the second segment, delivery of
	 * octets from the first segment shall not resume unless it appears in the
	 * live segment list more than once (which is permissible).
	 *
	 * Segments which occupy no sequence numbers must not appear in the live
	 * segment list.
	 *
	 * This arrangement uniquely specifies how the octet stream should be
	 * reassembled from the available segments (subject to revision should
	 * further segments be received before reassembly takes place). It
	 * follows that any decisions regarding the precedence of overlapping
	 * segments must be made before they are added to the list.
	 */
	segment_list _segments;

	/** True if this buffer assembles traffic flowing from the client to the
	 * server, false if flowing from the server to the client.
	 */
	bool _direction;

	/** The reassembly policy for overlapping segments. */
	std::unique_ptr<reassembly_policy> _policy;

	/** The current extended sequence number.
	 * This is the lowest extended sequence number for which data has not yet
	 * been delivered to the event listeners. If it has no value then no data
	 * has been delivered.
	 */
	std::optional<uint64_t> _curseq;

	/** A list of listeners which have registered to receive stream events.
	 * This is a pointer to a list object which would normally be shared
	 * with the connection to which this buffer belongs. Changes are allowed
	 * after the buffer has been constructed.
	 */
	std::list<octet::stream::listener*>* _listeners;

	/** Find the segment list entry providing a given sequence number.
	 * The result is provided in the same format as the equal_range function
	 * of a standard container. There can be at most one matching entry, and
	 * it is considered to be a match if:
	 * - the sequence number by which it is indexed is less than or equal to
	 *   the requested sequence number; and
	 * - it has a sufficiently long payload (including allowance for SYN/FIN
	 *   flags) to cover the requested sequence number and;
	 * - it is not pre-empted by another entry prior to the requested sequence
	 *   number.
	 * @param seq the requested sequence number
	 * @return a sequence defined by a pair of iterators containing the result
	 */
	std::pair<const_iterator, const_iterator> _equal_range(uint64_t seq) const;

	/** Report an event.
	 * @param ev the event to be reported
	 */
	void _report(const event& ev) const;
public:
	/** Construct TCP segment buffer.
	 * The list of listeners must remain in existence for the lifetime of the
	 * buffer object, since only a reference is kept. Changes to the content
	 * of the list object after construction of the buffer object will affect
	 * event delivery.
	 * @param direction true if assembling data flowing from client to server,
	 *  false if flowing from server to client
	 * @param policy the reassembly policy for overlapping segments
	 * @param listeners a list of registered stream event listeners
	 */
	segment_buffer(bool direction,
		std::list<octet::stream::listener*>* listeners,
		std::unique_ptr<reassembly_policy> policy = nullptr);

	/** Get the current extended sequence number.
	 * This is the lowest extended sequence number for which data has not yet
	 * been delivered to the event listeners. If it has no value then no data
	 * has been delivered.
	 * @return the current extended sequence number
	 */
	const std::optional<uint64_t>& curseq() const {
		return _curseq;
	}

	/** Insert TCP segment.
	 * @param new_seg the segment to be inserted
	 */
	void insert(const tcp::segment& new_seg);

	/** Deliver up to a given sequence number.
	 * This will stop at the first gap, unless delivery of gaps has been
	 * enabled.
	 * @param targetseq the sequence number up to which delivery is requested
	 * @param allow_gaps true to allow delivery of gaps, otherwise false
	 */
	void deliver(uint64_t targetseq, bool allow_gaps = false);

	/** Deliver everything available.
	 * This will stop at the first gap, unless delivery of gaps has been
	 * enabled.
	 * @param allow_gaps true to allow delivery of gaps, otherwise false
	 */
	void deliver(bool allow_gaps = false);

	/** Remove complete segments up to a given sequence number.
	 * @param targetseq the sequence number up to which removal is required
	 */
	void remove(uint64_t targetseq);
};

} /* namespace holmes::net::tcp */

#endif
