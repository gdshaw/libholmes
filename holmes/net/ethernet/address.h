// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_NET_ETHERNET_ADDRESS
#define HOLMES_NET_ETHERNET_ADDRESS

#include <string>
#include <iostream>

#include "holmes/octet/string.h"

namespace holmes::net::ethernet {

/** A class to represent an Ethernet address. */
class address {
private:
	/** The raw content of this address. */
	octet::string _data;
public:
	/** Construct Ethernet address from raw content.
	 * @param content the raw content
	 */
	explicit address(const octet::string& data);

	/** Get the raw content of this address.
	 * @return the raw content
	 */
	const octet::string& data() const {
		return _data;
	}

	/** Convert this IPv4 address to a string, in IEEE format.
	 * Each octet is converted to a pair of hex digits, using upper case
	 * where applicable. The resulting values are then concatenated in
	 * transmission order, separated by hyphens.
	 * @return the address as a string
	 */
	virtual operator std::string() const;
};

/** Write an Ethernet address to an output stream in IEEE format.
 * The format is the same as for octet::string::operator std::string(),
 * excpet that where there is a choice between upper and lower case this is
 * determined by the stream state.
 * @param out the output stream
 * @param octets the octet string
 * @return the output stream
 */
std::ostream& operator<<(std::ostream& out, const address& addr);

} /* namespace holmes::net::ethernet */

#endif
