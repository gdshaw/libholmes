// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "holmes/bson/int32.h"
#include "holmes/bson/binary.h"
#include "holmes/net/ethernet/frame.h"

namespace holmes::net::ethernet {

bson::object frame::to_bson() const {
	bson::object bson_datagram;
	bson_datagram.insert("dst_addr", dst_addr());
	bson_datagram.insert("src_addr", src_addr());
	bson_datagram.insert("ethertype", bson::int32(ethertype()));
	bson_datagram.insert("payload", bson::binary(payload()));
	return bson_datagram;
}

} /* namespace holmes::net::ethernet */
