// This file is part of libholmes.
// Copyright 2020-23 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_NET_DNS_LABEL
#define HOLMES_NET_DNS_LABEL

#include "holmes/octet/string.h"

namespace holmes::net::dns {

/** A class to represent an encoded domain name label. */
class label {
private:
	/** The raw content of this label, including the length field. */
	octet::string _data;
public:
	/** Decode domain name label.
	 * @param data the encoded domain name label
	 * @param length the length of the content of this label
	 */
	label(octet::string& data, size_t length);

	operator std::string() const;
};

} /* namespace holmes::net::dns */

#endif
