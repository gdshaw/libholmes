// This file is part of libholmes.
// Copyright 2020-23 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_NET_DNS_MESSAGE
#define HOLMES_NET_DNS_MESSAGE

#include "holmes/octet/string.h"
#include "holmes/artefact.h"

namespace holmes::net::dns {

/** A class to represent a DNS message. */
class message:
	public artefact {
private:
	/** The raw content. */
	octet::string _data;
public:
	/** Construct DNS message.
	 * @param data the raw content of the message
	 */
	message(const octet::string& data);

	bson::object to_bson() const override;

	/** Get the messaghe ID.
	 * @return the message ID
	*/
	uint16_t id() const {
		return get_uint16(_data, 0);
	}

	/** Get the query/response flag.
	 * @return false if a query, or true if a response
	 */
	bool qr() const {
		return (_data[2] >> 7) & 1;
	}

	/** Get the the opcode.
	 * @return the opcode
	 */
	unsigned int opcode() const {
		return (_data[2] >> 3) & 0x0f;
	}

	/** Get the authoritative answer flag.
	 * @return true if authoritative, otherwise false
	 */
	bool aa() const {
		return (_data[2] >> 2) & 1;
	}

	/** Get the truncation flag.
	 * @return true if truncated, otherwise false
	 */
	bool tc() const {
		return (_data[2] >> 1) & 1;
	}

	/** Get the recursion desired flag.
	 * @return true if recursion desired, otherwise false
	 */
	bool rd() const {
		return _data[2] & 1;
	}

	/** Get the recursion available flag.
	 * @return true if recursion available, otherwise false
	 */
	bool ra() const {
		return (_data[3] >> 7) & 1;
	}

	/** Get the response code..
	 * @return the response code
	 */
	unsigned int rcode() const {
		return _data[3] & 0x0f;
	}

	/** Get the number of questions.
	 * @return the number of questions
	 */
	uint16_t qdcount() const {
		return get_uint16(_data, 4);
	}

	/** Get the number of answers.
	 * @return the number of answers
	 */
	uint16_t ancount() const {
		return get_uint16(_data, 6);
	}

	/** Get the number of nameservers.
	 * @return the number of nameservers
	 */
	uint16_t nscount() const {
		return get_uint16(_data, 8);
	}

	/** Get the number of additional records.
	 * @return the number of additional records
	 */
	uint16_t arcount() const {
		return get_uint16(_data, 10);
	}
};

} /* namespace holmes::net::dns */

#endif
