// This file is part of libholmes.
// Copyright 2020-23 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_NET_DNS_NAME
#define HOLMES_NET_DNS_NAME

#include <vector>

#include "holmes/octet/string.h"
#include "holmes/net/dns/label.h"

namespace holmes::net::dns {

/** A class to represent an encoded domain name. */
class name {
private:
	/** The labels from which this name is composed. */
	std::vector<label> _labels;
public:
	/** Construct empty domain name. */
	name() = default;

	/** Decode domain name.
	 * @param data the encoded domain name
	 * @param msg the complete encoded DNS message
	 */
	name(octet::string& data, const octet::string& msg);

	operator std::string() const;
};

} /* namespace holmes::net::dns */

#endif
