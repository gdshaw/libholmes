// This file is part of libholmes.
// Copyright 2020-23 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include "holmes/net/dns/label.h"

namespace holmes::net::dns {

label::label(octet::string& data, size_t length) {
	if (data.length() < length) {
		throw std::out_of_range("truncated DNS label");
	}
	_data = read(data, length + 1);
}

label::operator std::string() const {
	size_t length = _data.length() - 1;
	const char* content = reinterpret_cast<const char*>(_data.data() + 1);
	return std::string(content, length);
}

} /* namespace holmes::net::dns */
