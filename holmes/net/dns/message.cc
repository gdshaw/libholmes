// This file is part of libholmes.
// Copyright 2020-23 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "holmes/parse_error.h"
#include "holmes/bson/boolean.h"
#include "holmes/bson/int32.h"
#include "holmes/bson/object.h"
#include "holmes/net/dns/message.h"

namespace holmes::net::dns {

using namespace bson;

message::message(const octet::string& data):
	_data(data) {}

bson::object message::to_bson() const {
	bson::object bson_message;
	bson_message.insert("id", bson::int32(id()));
	bson_message.insert("qr", bson::boolean(qr()));
	bson_message.insert("opcode", bson::int32(opcode()));
	bson_message.insert("aa", bson::boolean(aa()));
	bson_message.insert("tc", bson::boolean(tc()));
	bson_message.insert("rd", bson::boolean(rd()));
	bson_message.insert("ra", bson::boolean(ra()));
	bson_message.insert("rcode", bson::int32(rcode()));
	return bson_message;
}

} /* namespace holmes::net::dns */
