// This file is part of libholmes.
// Copyright 2020-23 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "holmes/parse_error.h"
#include "holmes/net/dns/name.h"

namespace holmes::net::dns {

name::name(octet::string& data, const octet::string& msg) {
	octet::string refdata;
	octet::string* source = &data;
	unsigned int depth = 0;

	uint8_t length = 0;
	do {
		length = get_uint8(*source, 0);
		switch (length & 0xc0) {
		case 0x00:
			_labels.push_back(label(*source, length));
			break;
		case 0x40:
			throw parse_error("EDNS labels not yet implemented");
		case 0x80:
			throw parse_error("unrecognised DNS label type");
		case 0xc0:
			size_t offset = read_uint16(*source) & 0x3fff;
			refdata = msg.substr(offset);
			source = &refdata;
			depth += 1;
			if (depth > 64) {
				throw parse_error(
					"recursive compressed DNS label");
			}
		}
	} while (length != 0);
}

name::operator std::string() const {
	std::string namestr;
	bool first = true;
	for (const auto& label : _labels) {
		if (first) {
			first = false;
		} else {
			namestr.push_back('.');
		}
		namestr.append(std::string(label));
	}
	return namestr;
}

} /* namespace holmes::net::dns */
