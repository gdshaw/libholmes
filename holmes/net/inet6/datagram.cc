// This file is part of libholmes.
// Copyright 2020-23 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <arpa/inet.h>

#include "holmes/bson/int32.h"
#include "holmes/bson/int64.h"
#include "holmes/bson/binary.h"
#include "holmes/net/inet/checksum.h"
#include "holmes/net/inet6/datagram.h"

namespace holmes::net::inet6 {

datagram::datagram(octet::string& data) {
	size_t length = 40;
	try {
		length += get_uint16(data, 4);
	} catch (std::out_of_range&) {
		/* no action */
	}
	_data = read(data, length);
}

bson::object datagram::to_bson() const {
	bson::object bson_datagram;
	bson_datagram.insert("version", bson::int32(version()));
	bson_datagram.insert("traffic_class", bson::int32(traffic_class()));
	bson_datagram.insert("flow_label", bson::int32(flow_label()));
	bson_datagram.insert("payload_length", bson::int64(payload_length()));
	bson_datagram.insert("next_header", bson::int32(next_header()));
	bson_datagram.insert("hop_limit", bson::int32(hop_limit()));
	bson_datagram.insert("src_addr", src_addr());
	bson_datagram.insert("dst_addr", dst_addr());
	bson_datagram.insert("payload", bson::binary(payload()));
	return bson_datagram;
}

inet::checksum datagram::make_pseudo_header_checksum(
	uint8_t protocol, size_t length) const {

        inet::checksum checksum;
        checksum(_data.substr(8, 32));
	checksum(length >> 16);
	checksum(length >> 0);
	checksum(0);
	checksum(protocol);
        return checksum;
}

} /* namespace holmes::net::inet6 */
