// This file is part of libholmes.
// Copyright 2020-23 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_NET_INET6_ADDRESS
#define HOLMES_NET_INET6_ADDRESS

#include "holmes/net/inet/address.h"

namespace holmes::net::inet6 {

/** A class to represent an IPv6 address. */
class address:
	public inet::address {
protected:
	address* _clone() const override;
public:
	/** Construct IPv4 address from raw content.
	 * @param data the raw content
	 */
	explicit address(const octet::string& data);

	operator std::string() const override;

	std::unique_ptr<address> clone() {
		return std::unique_ptr<address>(_clone());
	}
};

} /* namespace holmes::net::inet4 */

#endif
