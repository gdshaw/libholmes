// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_NET_TELNET_COMMAND_EVENT
#define HOLMES_NET_TELNET_COMMAND_EVENT

#include "holmes/net/telnet/event.h"

namespace holmes::net::telnet {

/** A class to represent transmission of a Telnet command. */
class command_event:
	public event {
private:
	/** The command code. */
	unsigned char _command;
public:
	/** Construct Telnet command event.
	 * @param direction true if direction of travel is client to server,
	 *  false if server to client
	 * @param command the command code
	 */
	command_event(bool direction, unsigned char command):
		event(octet::string(), direction),
		_command(command) {}

	/** Get the command code. */
	unsigned char command() const {
		return _command;
	}
};

} /* namespace holmes::net::telnet */

#endif
