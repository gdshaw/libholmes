// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "holmes/net/telnet/command_event.h"
#include "holmes/net/telnet/negotiate_event.h"
#include "holmes/net/telnet/peer.h"

namespace holmes::net::telnet {

enum {
	code_se = 240,
	code_nop = 241,
	code_dm = 242,
	code_brk = 243,
	code_ip = 244,
	code_ao = 245,
	code_ayt = 246,
	code_ec = 247,
	code_el = 248,
	code_ga = 249,
	code_sb = 250,
	code_will = 251,
	code_wont = 252,
	code_do = 253,
	code_dont = 254,
	code_iac = 255
};

void peer::_report(const octet::stream::event& ev) const {
	for (auto listener : *_listeners) {
		listener->handle(ev);
	}
}

peer::peer(bool server, peer& peer,
	std::list<octet::stream::listener*>* listeners):
	_server(server),
	_peer(&peer),
	_state(state_default),
	_listeners(listeners) {}

telnet::option* peer::option(uint8_t code, bool create) {
	auto f = _options.find(code);
	if (f != _options.end()) {
		return f->second.get();
	}
	if (!create) {
		return 0;
	}
	return (_options[code] = telnet::option::make(code)).get();
}

void peer::flush() {
	if (!_buffer.empty()) {
		_report(octet::stream::event(octet::string(_buffer), _server));
		_buffer.clear();
	}
}

void peer::handle_sb(unsigned char code,
	std::basic_string<unsigned char> data) {

	subnegotiation_event ev(_server, code_sb, code, data);
	_report(ev);
	option(code, true)->handle(ev, false);
	_peer->option(code, true)->handle(ev, true);
}

void peer::handle_sb_opt(unsigned char b) {
	_buffer.push_back(b);
	_state = state_sb_param;
}

void peer::handle_sb_param(unsigned char b) {
	if (b == code_iac) {
		_state = state_sb_iac;
	} else {
		_buffer.push_back(b);
	}
}

void peer::handle_sb_iac(unsigned char b) {
	if (b == code_se) {
		handle_sb(_buffer.front(), _buffer.substr(1));
		_buffer.clear();
		_state = state_default;
	} else {
		_buffer.push_back(b);
		_state = state_sb_param;
	}
}

void peer::handle_negotiate(unsigned char command, unsigned char b,
	bool remote) {

	if (!remote) {
		switch (command) {
		case code_will:
			option(b, true)->negotiate(false, true);
			break;
		case code_wont:
			option(b, true)->negotiate(false, false);
			break;
		}
		_report(negotiate_event(_server, command, b));
		_state = state_default;
	} else {
		switch (command) {
		case code_do:
			option(b, true)->negotiate(true, true);
			break;
		case code_dont:
			option(b, true)->negotiate(true, false);
			break;
		}
	}
}

void peer::handle_iac(unsigned char b) {
	switch (b) {
	case code_nop:
	case code_dm:
	case code_brk:
	case code_ip:
	case code_ao:
	case code_ayt:
	case code_ec:
	case code_el:
	case code_ga:
		_report(command_event(_server, b));
		break;
	case code_sb:
		_state = state_sb_opt;
		break;
	case code_will:
		_state = state_will;
		break;
	case code_wont:
		_state = state_wont;
		break;
	case code_do:
		_state = state_do;
		break;
	case code_dont:
		_state = state_dont;
		break;
	default:
		_buffer.push_back(b);
		_state = state_default;
		break;
	}
}

void peer::handle(unsigned char b) {
	switch (_state) {
	case state_default:
		if (b == code_iac) {
			flush();
			_state = state_iac;
		} else {
			_buffer.push_back(b);
		}
		break;
	case state_iac:
		handle_iac(b);
		break;
	case state_will:
		handle_negotiate(code_will, b, false);
		_peer->handle_negotiate(code_will, b, true);
		break;
	case state_wont:
		handle_negotiate(code_wont, b, false);
		_peer->handle_negotiate(code_wont, b, true);
		break;
	case state_do:
		handle_negotiate(code_do, b, false);
		_peer->handle_negotiate(code_do, b, true);
		break;
	case state_dont:
		handle_negotiate(code_dont, b, false);
		_peer->handle_negotiate(code_dont, b, true);
		break;
	case state_sb_opt:
		handle_sb_opt(b);
		break;
	case state_sb_param:
		handle_sb_param(b);
		break;
	case state_sb_iac:
		handle_sb_iac(b);
		break;
	}
}

void peer::handle(const octet::stream::event& ev) {
	const octet::string payload = ev.payload();
	for (size_t i = 0; i != payload.length(); ++i) {
		handle(payload[i]);
	}
	flush();
}

bson::object peer::to_bson() const {
	bson::object bson_options;
	for (const auto& i : _options) {
		bson_options.insert(std::to_string(i.first),
			i.second->to_bson());
	}

	bson::object bson_peer;
	bson_peer.insert("options", bson_options);
	return bson_peer;
}

} /* namespace holmes::net::telnet */
