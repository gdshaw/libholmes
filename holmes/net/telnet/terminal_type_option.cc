// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include<algorithm>

#include "holmes/bson/array.h"
#include "holmes/bson/string.h"
#include "holmes/net/telnet/terminal_type_option.h"

namespace holmes::net::telnet {

enum {
	code_is = 0,
	code_send = 1
};

terminal_type_option::terminal_type_option():
	option(24) {}

void terminal_type_option::handle_is(const subnegotiation_event& ev) {
	auto& params = ev.parameters();
	_termtype = std::string(params.begin() + 1, params.end());
	if (std::find(_termtypes.begin(), _termtypes.end(), *_termtype) ==
		_termtypes.end()) {

		_termtypes.push_back(*_termtype);
	}
}

void terminal_type_option::handle_send(const subnegotiation_event& ev) {
	/** No action */
}

void terminal_type_option::handle(const subnegotiation_event& ev,
	bool remote) {

	auto& params = ev.parameters();
	if (params.length() >= 1) {
		switch (params[0]) {
		case code_is:
			if (!remote) {
				handle_is(ev);
			}
			break;
		case code_send:
			if (remote) {
				handle_send(ev);
			}
			break;
		default:
			handle_error(ev);
			break;
		}
	} else {
		handle_error(ev);
	}
}

bson::object terminal_type_option::to_bson() const {
	bson::array bson_termtypes;
	for (auto termtype : _termtypes) {
		bson_termtypes.append(bson::string(termtype));
	}

	bson::object bson_option = option::to_bson();
	if (_termtype) {
		bson_option.insert("termtype", *_termtype);
	}
	if (!_termtypes.empty()) {
		bson_option.insert("termtypes", bson_termtypes);
	}
	return bson_option;
}

} /* namespace holmes::net::telnet */
