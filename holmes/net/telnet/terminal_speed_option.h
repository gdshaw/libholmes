// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_NET_TELNET_TERMINAL_SPEED_OPTION
#define HOLMES_NET_TELNET_TERMINAL_SPEED_OPTION

#include <optional>
#include <string>

#include "holmes/net/telnet/option.h"
#include "holmes/net/telnet/subnegotiation_event.h"

namespace holmes::net::telnet {

/** A class to represent a Telnet terminal speed option (code 32). */
class terminal_speed_option:
	public option {
private:
	/** The current terminal speed. */
	std::optional<std::string> _speed;
public:
	/** Construct terminal speed option. */
	terminal_speed_option();

	void handle_is(const subnegotiation_event& ev);
	void handle_send(const subnegotiation_event& ev);

	virtual void handle(const subnegotiation_event& ev, bool remote);
	virtual bson::object to_bson() const;
};

} /* namespace holmes::net::telnet */

#endif
