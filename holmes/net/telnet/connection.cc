// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "holmes/net/telnet/connection.h"

namespace holmes::net::telnet {

connection::connection():
	_client(false, _server, &_listeners),
	_server(true, _client, &_listeners) {}

void connection::handle(const octet::stream::event& ev) {
	if (ev.direction()) {
		_client.handle(ev);
	} else {
		_server.handle(ev);
	}
}

void connection::flush() {
	_client.flush();
	_server.flush();
}

void connection::add_listener(octet::stream::listener& listener) {
	_listeners.push_back(&listener);
}

bson::object connection::to_bson() const {
	bson::object bson_connection;
	bson_connection.insert("client", _client.to_bson());
	bson_connection.insert("server", _server.to_bson());
	return bson_connection;
}

} /* namespace holmes::net::telnet */
