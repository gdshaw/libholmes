// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_NET_TELNET_CONNECTION
#define HOLMES_NET_TELNET_CONNECTION

#include "holmes/bson/object.h"
#include "holmes/octet/stream/event.h"
#include "holmes/octet/stream/listener.h"
#include "holmes/net/telnet/peer.h"

namespace holmes::net::telnet {

class connection:
	public octet::stream::listener {
private:
	/** An object to represent the state of the client. */
	peer _client;

	/** An object to represent the state of the server. */
	peer _server;

	/** A list of registered event listeners. */
	std::list<octet::stream::listener*> _listeners;
public:
	/** Construct telnet connection. */
	connection();

	virtual void handle(const octet::stream::event& ev);

	peer& client() {
		return _client;
	}

	peer& server() {
		return _server;
	}

	/** Flush any buffered content. */
	void flush();

	/** Register an event listener to receive interpreted content.
	 * @param listener the event listener to be registered
	 */
	void add_listener(octet::stream::listener& listener);

	/** Convert the state of this connection to BSON.
	 * @return a BSON representation of this connection.
	 */
	virtual bson::object to_bson() const;
};

} /* namespace holmes::net::telnet */

#endif
