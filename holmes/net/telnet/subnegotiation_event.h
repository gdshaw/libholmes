// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_NET_TELNET_SUBNEGOTIATION_EVENT
#define HOLMES_NET_TELNET_SUBNEGOTIATION_EVENT

#include "holmes/net/telnet/command_event.h"

namespace holmes::net::telnet {

class subnegotiation_event:
	public command_event {
private:
	/** The option number. */
	unsigned char _option;

	/** The subnegotiation parameters. */
	std::basic_string<unsigned char> _params;
public:
	/** Construct Telnet subnegotiation event.
	 * @param direction true if direction of travel is client to server,
	 *  false if server to client
	 * @param command the command code
	 * @param option the option code
	 * @param params the subnegotiation parameters
	 */
	subnegotiation_event(bool direction, unsigned char command,
		unsigned char option, const std::basic_string<unsigned char>& params):
		command_event(direction, command),
		_option(option),
		_params(params) {}

	/** Get the option code.
	 * @return the option code
	 */
	unsigned char option() const {
		return _option;
	}

	/** Get the subnegotiation parameters.
	 * @return the parameters
	 */
	const std::basic_string<unsigned char>& parameters() const {
		return _params;
	}
};

} /* namespace holmes::net::telnet */

#endif
