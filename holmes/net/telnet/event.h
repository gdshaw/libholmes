// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_NET_TELNET_EVENT
#define HOLMES_NET_TELNET_EVENT

#include "holmes/octet/stream/event.h"

namespace holmes::net::telnet {

class event:
	public octet::stream::event {
public:
	/** Construct Telnet event.
	 * @param payload the in-band payload, if any
	 * @param server true if the event refers to the server, false if it
	 *  refers to the client
	 */
	event(const octet::string& payload, bool direction):
		octet::stream::event(payload, direction) {}
};

} /* namespace holmes::net::telnet */

#endif
