// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "holmes/bson/boolean.h"
#include "holmes/net/telnet/option.h"
#include "holmes/net/telnet/terminal_type_option.h"
#include "holmes/net/telnet/terminal_speed_option.h"
#include "holmes/net/telnet/x_display_location_option.h"

namespace holmes::net::telnet {

void option::negotiate(bool remote, bool enable) {
	if (remote) {
		_do = enable;
	} else {
		_will = enable;
	}
}

void option::handle_error(const subnegotiation_event& ev) {
	_error = true;
}

void option::handle(const subnegotiation_event& ev, bool remote) {}

bson::object option::to_bson() const {
	bson::object bson_option;
	if (_will) {
		bson_option.insert("will", bson::boolean(*_will));
	}
	if (_do) {
		bson_option.insert("do", bson::boolean(*_do));
	}
	if (_error) {
		bson_option.insert("error", bson::boolean(true));
	}
	return bson_option;
}

std::unique_ptr<option> option::make(uint8_t code) {
	switch (code) {
	case 24:
		return std::make_unique<terminal_type_option>();
	case 32:
		return std::make_unique<terminal_speed_option>();
	case 35:
		return std::make_unique<x_display_location_option>();
	default:
		return std::make_unique<option>(code);
	}
}

} /* namespace holmes::net::telnet */
