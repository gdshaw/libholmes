// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_NET_TELNET_OPTION
#define HOLMES_NET_TELNET_OPTION

#include <cstdint>
#include <memory>
#include <optional>

#include "holmes/bson/object.h"
#include "holmes/net/telnet/subnegotiation_event.h"

namespace holmes::net::telnet {

/** A base class to represent the state of a Telnet option. */
class option {
private:
	/** The option code. */
	uint8_t _code;

	/** True if the local host has send a WILL, false if it has sent a WON'T,
	 * or unset if it has sent neither.
	 */
	std::optional<bool> _will;

	/** True if the remote host has send a DO, false if it has sent a DON'T,
	 * or unset if it has sent neither.
	 */
	std::optional<bool> _do;

	/** The error indicator.
	 * This is set to true if a subnegotiation is observed which cannot be
	 * handled.
	 */
	bool _error = false;
public:
	/** Construct an option.
	 * @param code the option code
	 */
	option(uint8_t code):
		_code(code) {}

	/** Destroy this option. */
	virtual ~option() = default;

	/** Get the option code.
	 * @return the option code
	 */
	uint8_t code() const {
		return _code;
	}

	/** Test whether this option is currently enabled.
	 * @return true if enabled, otherwise false
	 */
	bool enabled() const {
		return _will && _do && *_will && *_do;
	}

	/** Get the error indicator.
	 * @return true if an error has occurred, otherwise false
	 */
	bool error() const {
		return _error;
	}

	/** Report a subnegotiation event which cannot be handled. */
	void handle_error(const subnegotiation_event& ev);

	/** Handle negotiation command (WILL, WON'T, DO, DON'T).
	 * @param remote true if sent by remote host, false if local host
	 * @param enable true to enable option, false to disable
	 */
	void negotiate(bool remote, bool enable);

	/** Handle subnegotiation event.
	 * @param ev the event to be handled
	 * @param remote true if the event originated from the remote host,
	 *  false if the local host
	 */
	virtual void handle(const subnegotiation_event& ev, bool remote);

	/** Convert this option to BSON.
	 * @return a BSON representation of this option.
	 */
	virtual bson::object to_bson() const;

	/** Make an option with a given option code.
	 * Depending on the option code, the result may be an instance of an
	 * appropriate subclass.
	 * @return the resulting instance
	 */
	static std::unique_ptr<option> make(uint8_t code);
};

} /* namespace holmes::net::telnet */

#endif
