// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_NET_TELNET_NEGOTIATE_EVENT
#define HOLMES_NET_TELNET_NEGOTIATE_EVENT

#include "holmes/net/telnet/command_event.h"

namespace holmes::net::telnet {

class negotiate_event:
	public command_event {
private:
	/** The option number. */
	unsigned char _option;
public:
	/** Construct Telnet negotiate event.
	 * @param direction true if direction of travel is client to server,
	 *  false if server to client
	 * @param command the command code
	 * @param option the option code
	 */
	negotiate_event(bool direction, unsigned char command,
		unsigned char option):
		command_event(direction, command),
		_option(option) {}

	/** Get the option code.
	 * @return the option code
	 */
	unsigned char option() const {
		return _option;
	}
};

} /* namespace holmes::net::telnet */

#endif
