// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_NET_TELNET_TERMINAL_TYPE_OPTION
#define HOLMES_NET_TELNET_TERMINAL_TYPE_OPTION

#include <optional>
#include <list>
#include <string>

#include "holmes/net/telnet/option.h"
#include "holmes/net/telnet/subnegotiation_event.h"

namespace holmes::net::telnet {

/** A class to represent a Telnet terminal type option (code 24).
 * This was originally specified in RFC 884, which allowed the terminal type
 * to be transmitted spontanously once the option had been negotiated, and
 * assumed the terminal type to be single-valued.
 * RFC 930 forbade spontaneous transmission of the terminal type.
 * RFC 1091 permitted cycling through a list of terminal types
 */
class terminal_type_option:
	public option {
private:
	/** The current reported terminal type. */
	std::optional<std::string> _termtype;

	/** A list of all reported terminal types.
	 * These are listed in the order in which they were observed.
	 */
	std::list<std::string> _termtypes;

	enum {
		code_is = 0,
		code_send = 1
	};
public:
	/** Construct terminal type option. */
	terminal_type_option();

	/** Get the current reported terminal type, if any.
	 * @return the current terminal type
	 */
	auto termtype() const {
		return _termtype;
	}

	/** Get a list of all reported terminal types.
	 * These are listed in the order in which they were observed.
	 * @return a list of terminal types
	 */
	auto termtypes() const {
		return _termtypes;
	}

	/** Handle a local IS command.
	 * @param ev the subnegotiation event containing the command
	 */
	void handle_is(const subnegotiation_event& ev);

	/** Handle a remote SEND command.
	 * @param ev the subnegotiation event containing the command
	 */
	void handle_send(const subnegotiation_event& ev);

	virtual void handle(const subnegotiation_event& ev, bool remote);
	virtual bson::object to_bson() const;
};

} /* namespace holmes::net::telnet */

#endif
