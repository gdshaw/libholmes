// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_NET_TELNET_PEER
#define HOLMES_NET_TELNET_PEER

#include <list>
#include <map>
#include <string>

#include "holmes/bson/object.h"
#include "holmes/octet/stream/event.h"
#include "holmes/octet/stream/listener.h"
#include "holmes/net/telnet/option.h"

namespace holmes::net::telnet {

/** A class to represent one of the parties to a telnet connection. */
class peer:
	public octet::stream::listener {
private:
	/** True if this peer is the server, false if the client. */
	bool _server;

	/** The host at the remote end of the connection. */
	peer* _peer;

	/** The current state.
	 * This determines how the next character is interpreted.
	 */
	enum {
		/** The default state, in which all characters except IAC are
		 * interpreted as content. */
		state_default,
		/** The state in which an IAC character has been received. */
		state_iac,
		/** The state in which a WILL command is awaiting the option code. */
		state_will,
		/** The state in which a WON'T command is awaiting the option code. */
		state_wont,
		/** The state in which a DO command is awaiting the option code. */
		state_do,
		/** The state in which a DON'T command is awaiting the option code. */
		state_dont,
		/** The state in which a subnegotiation is awaiting the option code. */
		state_sb_opt,
		/** The state in which a subnegotiation is awaiting parameter data. */
		state_sb_param,
		/** The state in which a subnegotiation has encountered an IAC. */
		state_sb_iac
	} _state;

	/** Any buffered content which has not yet been reported.
	 * Characters are placed in this buffer only if they have been
	 * interpreted as content. The buffer must be flushed before
	 * reporting a command, to preserve ordering. */
	std::basic_string<unsigned char> _buffer;

	/** A list of listeners which have registered to receive stream events.
	 * This is a pointer to a list object which would normally be shared
	 * with the connection to which this peer belongs. Changes are allowed
	 * after the buffer has been constructed.
	 */
	std::list<octet::stream::listener*>* _listeners;

	/** Report an event.
	 * @param ev the event to be reported
	 */
	void _report(const octet::stream::event& ev) const;

	/** Observed options, indexed by option code.
	 * Inclusion of an object in this map does not necessarily imply that it
	 * is or ever has been enabled, only that a negotiation or subnegotiation
	 * command referring to it has been seen.
	 */
	std::map<uint8_t, std::unique_ptr<telnet::option>> _options;
public:
	/** Construct telnet peer.
	 * @param server true if the peer is the server, false if the client
	 * @param peer the host at the remote end of the connection
	 * @param listeners a list of registered stream event listeners
	 */
	peer(bool server, peer& peer,
		std::list<octet::stream::listener*>* listeners);

	/** Get all observed options, indexed by option code.
	 * @return a container
	 */
	auto& options() const {
		return _options;
	}

	/** Get the option for a specified option code.
	 * @param code the option code
	 * @param create true to create the option if it does not already exist,
	 *  otherwise false
	 */
	telnet::option* option(uint8_t code, bool create = false);

	/** Flush any buffered content. */
	void flush();

	/** Handle a completed subnegotiation.
	 * @param code the option code
	 * @param data the parameter data to be handled
	 */
	void handle_sb(unsigned char code,
		std::basic_string<unsigned char> data);

	/** Handle a subnegotiation awaiting an option code.
	 * @param b the octet to be handled
	 */
	void handle_sb_opt(unsigned char b);

	/** Handle a subnegotiation awaiting parameter data.
	 * @param b the octet to be handled
	 */
	void handle_sb_param(unsigned char b);

	/** Handle a subnegotiation following an IAC.
	 * @param b the octet to be handled
	 */
	void handle_sb_iac(unsigned char b);

	/** Handle an octet in state_will, wont, do or dont.
	 * @param command the command in progress
	 * @param b the octet to be handled
	 * @param remote true if the command was sent by the remote host,
	 *  otherwise false
	 */
	void handle_negotiate(unsigned char command, unsigned char b, bool remote);

	/** Handle an octet in state_iac.
	 * @param b the octet to be handled
	 */
	void handle_iac(unsigned char b);

	/** Handle a single octet.
	 * @param b the octet to be handled
	 */
	void handle(unsigned char b);

	/** Convert the state of this host to BSON.
	 * @return a BSON representation of this host.
	 */
	virtual bson::object to_bson() const;

	virtual void handle(const octet::stream::event& ev);
};

} /* namespace holmes::net::telnet */

#endif
