// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "holmes/bson/array.h"
#include "holmes/bson/string.h"
#include "holmes/net/telnet/x_display_location_option.h"

namespace holmes::net::telnet {

enum {
	code_is = 0,
	code_send = 1
};

x_display_location_option::x_display_location_option():
	option(35) {}

void x_display_location_option::handle_is(const subnegotiation_event& ev) {
	auto& params = ev.parameters();
	_display = std::string(params.begin() + 1, params.end());
}

void x_display_location_option::handle_send(const subnegotiation_event& ev) {
	/** No action */
}

void x_display_location_option::handle(const subnegotiation_event& ev,
	bool remote) {

	auto& params = ev.parameters();
	if (params.length() >= 1) {
		switch (params[0]) {
		case code_is:
			if (!remote) {
				handle_is(ev);
			}
			break;
		case code_send:
			if (remote) {
				handle_send(ev);
			}
			break;
		default:
			handle_error(ev);
			break;
		}
	} else {
		handle_error(ev);
	}
}

bson::object x_display_location_option::to_bson() const {
	bson::object bson_option = option::to_bson();
	if (_display) {
		bson_option.insert("display", *_display);
	}
	return bson_option;
}

} /* namespace holmes::net::telnet */
