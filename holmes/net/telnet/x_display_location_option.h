// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_NET_TELNET_X_DISPLAY_LOCATION_OPTION
#define HOLMES_NET_TELNET_X_DISPLAY_LOCATION_OPTION

#include <optional>
#include <string>

#include "holmes/net/telnet/option.h"

namespace holmes::net::telnet {

/** A class to represent a Telnet X display location option (code 35). */
class x_display_location_option:
	public option {
private:
	/** The current X display location. */
	std::optional<std::string> _display;

	enum {
		code_is = 0,
		code_send = 1
	};
public:
	/** Construct X display location option. */
	x_display_location_option();

	void handle_is(const subnegotiation_event& ev);
	void handle_send(const subnegotiation_event& ev);

	virtual void handle(const subnegotiation_event& ev, bool remote);
	virtual bson::object to_bson() const;
};

} /* namespace holmes::net::telnet */

#endif
