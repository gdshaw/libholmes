// This file is part of libholmes.
// Copyright 2020-23 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_NET_INET4_ADDRESS
#define HOLMES_NET_INET4_ADDRESS

#include <iostream>

#include "holmes/net/inet/address.h"

namespace holmes::net::inet4 {

/** A class to represent an IPv4 address. */
class address:
	public inet::address {
protected:
	address* _clone() const override;
public:
	/** Construct IPv4 address from raw content.
	 * @param data the raw content
	 */
	explicit address(const octet::string& data);

	operator std::string() const override;

	std::unique_ptr<address> clone() {
		return std::unique_ptr<address>(_clone());
	}
};

/** Write an IPv4 address to an output stream, in dotted quad format.
 * The stream state is respected. If a field width has been specified,
 * it applies individually to each of the four components of the address
 * (but does not remain in effect following the final component).
 * @param out the output stream
 * @param addr the IPv4 address
 * @return the output stream
 */
std::ostream& operator<<(std::ostream& out, const address& addr);

} /* namespace holmes::net::inet4 */

#endif
