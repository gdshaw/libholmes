// This file is part of libholmes.
// Copyright 2020-23 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_NET_ADDRESS
#define HOLMES_NET_ADDRESS

#include <memory>
#include <string>
#include <iostream>

#include "holmes/octet/string.h"

namespace holmes::net {

/** A base class to represent a generic address. */
class address {
private:
	/** The raw content of this address. */
	octet::string _data;
protected:
	virtual address* _clone() const = 0;
public:
	/** Construct address from raw content.
	 * @param data the raw content
	 */
	explicit address(const octet::string& data):
		_data(data) {}

	/** Get the raw content of this address.
	 * @return the raw content
	 */
	const octet::string& data() const {
		return _data;
	}

	/** Convert this address to a string.
	 * @return the address as a string
	 */
	virtual operator std::string() const = 0;

	std::unique_ptr<address> clone() {
		return std::unique_ptr<address>(_clone());
	}
};

inline std::ostream& operator<<(std::ostream& out, const address& addr) {
	out << std::string(addr);
	return out;
}

} /* namespace holmes::net::inet4 */

#endif
