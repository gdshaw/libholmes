// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_ARTEFACT
#define HOLMES_ARTEFACT

#include "holmes/bson/object.h"

namespace holmes {

/** A class to represent a decoded artefact. */
class artefact {
public:
	/** Convert this object to BSON.
	 * @return a BSON representation of this artefact.
	 */
	virtual bson::object to_bson() const = 0;
};

} /* namespace holmes */

#endif
