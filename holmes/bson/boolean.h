// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_BSON_BOOLEAN
#define HOLMES_BSON_BOOLEAN

#include "holmes/bson/value.h"

namespace holmes::bson {

/** A BSON class to represent a boolean. */
class boolean:
	public value {
private:
	/** The boolean value. */
	bool _value;
public:
	/** Construct BSON value containing boolean.
	 * @param value the required boolean value
	 */
	explicit boolean(bool value);

	virtual std::unique_ptr<value> clone() const;
	virtual unsigned char type() const;
	virtual size_t length() const;
	virtual void _encode(writer& bw) const;
	virtual std::string to_json() const;

	/** Decode instance from octet string.
	 * @param bd the BSON data to be decoded
	 */
	static std::unique_ptr<boolean> decode(octet::string& bd);
};

} /* namespace holmes::bson */

#endif

