// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_BSON_VALUE
#define HOLMES_BSON_VALUE

#include <cstddef>
#include <memory>
#include <string>
#include <iostream>

#include "holmes/octet/string.h"

namespace holmes::bson {

class writer;
class any;

/** An abstract base class to represent a BSON value of any type. */
class value {
public:
	virtual ~value() = default;

	/** Make a copy of this BSON value with the same dynamic type.
	 * @return a copy of this value
	 */
	virtual std::unique_ptr<value> clone() const = 0;

	/** Get the type code for this value.
	 * @return the type code
	 */
	virtual unsigned char type() const = 0;

	/** Get the encoded length of this value.
	 * @return the length, in octets
	 */
	virtual size_t length() const = 0;

	/** Test whether this value is null.
	 * @return true if null, otherwise false
	 */
	virtual bool is_null() const;

	/** Access the member with a given key.
	 * If this value does not support indexing by string, or if the
	 * specified key does not exist, then an exception is throw.
	 * @param index the requested key
	 * @return the member corresponding to the requested key
	 */
	virtual any& at(const std::string& key);

	/** Access the member with a given key.
	 * If this value does not support indexing by string, or if the
	 * specified key does not exist, then an exception is throw.
	 * @param index the requested key
	 * @return the member corresponding to the requested key
	 */
	virtual const any& at(const std::string& key) const;

	/** Access the member with a given key.
	 * If this value does not support indexing by string then an
	 * exception is throw.
	 * If the specified key is not present then a member with that
	 * key is inserted with a default value of null.
	 */
	virtual any& operator[](const std::string& key);

	/** Access the member with a given index.
	 * If this value does not support indexing by integer, or if the
	 * specified index is out of range, then an exception is throw.
	 * @param index the requested index
	 * @return the member corresponding to the requested index
	 */
	virtual any& at(size_t index);

	/** Access the member with a given index.
	 * If this value does not support indexing by integer, or if the
	 * specified index is out of range, then an exception is throw.
	 * @param index the requested index
	 * @return the member corresponding to the requested index
	 */
	virtual const any& at(size_t index) const;

	/** Access the member with a given index.
	 * This function is equivalent to this->at(index).
	 * @param index the requested index
	 * @return the member corresponding to the requested index
	 */
	any& operator[](size_t index) {
		return at(index);
	}

	/** Access the member with a given index.
	 * This function is equivalent to this->at(index).
	 * @param index the requested index
	 * @return the member corresponding to the requested index
	 */
	const any& operator[](size_t index) const {
		return at(index);
	}

	/** Encode this value as BSON.
	 * @param bw a writer object to receive the BSON
	 */
	virtual void _encode(writer& bw) const = 0;

	/** Encode this value as BSON.
	 * @return the value as BSON
	 */
	octet::string encode() const;

	/** Encode this value as extended JSON.
	 * @return the encoded value
	 */
	virtual std::string to_json() const = 0;

	/** Decode a value from an octet string.
	 * @param type the required object type
	 * @param bd the BSON data to be decoded
	 * @return the resulting BSON value
	 */
	static std::unique_ptr<value> decode(unsigned char type,
		octet::string& bd);
};

inline std::ostream& operator<<(std::ostream& out, const bson::value& value) {
	return out << value.to_json();
}

} /* namespace holmes::bson */

#include "holmes/bson/any.h"

#endif
