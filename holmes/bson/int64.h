// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_BSON_INT64
#define HOLMES_BSON_INT64

#include <cstdint>

#include "holmes/bson/value.h"

namespace holmes::bson {

/** A BSON class to represent a 64-bit signed integer. */
class int64:
	public value {
private:
	/** The integer value. */
	int64_t _value;
public:
	/** Construct BSON value containing 64-bit integer.
	 * @param value the required integer value
	 */
	explicit int64(int64_t value);

	virtual std::unique_ptr<value> clone() const;
	virtual unsigned char type() const;
	virtual size_t length() const;
	virtual void _encode(writer& bw) const;
	virtual std::string to_json() const;

	/** Decode instance from octet string.
	 * @param bd the BSON data to be decoded
	 */
	static std::unique_ptr<int64> decode(octet::string& bd);
};

} /* namespace holmes::bson */

#endif
