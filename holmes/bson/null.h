// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_BSON_NULL
#define HOLMES_BSON_NULL

#include "holmes/bson/value.h"

namespace holmes::bson {

/** A BSON class to represent the null value. */
class null:
	public value {
public:
	virtual std::unique_ptr<value> clone() const;
	virtual unsigned char type() const;
	virtual size_t length() const;
	virtual bool is_null() const;
	virtual void _encode(writer& bw) const;
	virtual std::string to_json() const;
};

} /* namespace holmes::bson */

#endif
