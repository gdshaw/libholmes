// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_BSON_WRITER
#define HOLMES_BSON_WRITER

#include <cstddef>
#include <cstdint>

namespace holmes::bson {

/** A class for writing BSON data to a buffer. */
class writer {
private:
	/** A pointer to the next octet to be written. */
	unsigned char* _ptr;

	/** The number of octets remaining. */
	size_t _remaining;
public:
	/** Construct BSON writer.
	 * @param buffer the buffer to be written into
	 * @param size the size of the buffer, in octets
	 */
	writer(unsigned char* buffer, size_t size):
		_ptr(buffer),
		_remaining(size) {}

	/** Write a single byte.
	 * @param value the value to be written
	 */
	void write_byte(unsigned char value);

	/** Write a sequence of bytes.
	 * @param data the bytes to be written
	 * @param count the number of bytes to be written
	 */
	void write_bytes(const void* data, size_t count);

	/** Write an int32.
	 * @param value the value to be written
	 */
	void write_int32(int32_t value);

	/** Write an int64.
	 * @param value the value to be written
	 */
	void write_int64(int64_t value);

	/** Write a null-terminated string.
	 * @param value the value to be written
	 */
	void write_cstring(const char* value);
};

} /* namespace holmes::bson */

#endif
