// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_BSON_OBJECT
#define HOLMES_BSON_OBJECT

#include <vector>
#include <string>

#include "holmes/bson/value.h"

namespace holmes::bson {

/** A BSON class to represent an object. */
class object:
	public value {
public:
	/** The key type for this object. */
	typedef std::string key_type;

	/** The type to which keys are mapped by this object. */
	typedef bson::any mapped_type;

	/** The type of members of this object. */
	typedef std::pair<const key_type, mapped_type> value_type;
private:
	/** The members of this object. */
	std::vector<value_type> _members;
public:
	virtual std::unique_ptr<value> clone() const;
	virtual unsigned char type() const;
	virtual size_t length() const;
	virtual void _encode(writer& bw) const;
	virtual std::string to_json() const;
	virtual mapped_type& at(const std::string& name);
	virtual const mapped_type& at(const std::string& name) const;
	virtual mapped_type& operator[](const std::string& name);

	/** Insert a member into this object.
	 * If there is already a member with the specified name then it will
	 * be replaced.
	 * @param name the name of the member
	 * @param value the value of the member
	 */
	void insert(const std::string& name, const bson::value& value);

	/** Insert a string member into this object.
	 * If there is already a member with the specified name then it will
	 * be replaced.
	 * @param name the name of the member
	 * @param value the value of the string
	 */
	void insert(const std::string& name, const std::string& value);

	/** Decode instance from octet string.
	 * @param bd the BSON data to be decoded
	 */
	static std::unique_ptr<object> decode(octet::string& bd);
};

} /* namespace holmes::bson */

#endif
