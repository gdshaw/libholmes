// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_BSON_STRING
#define HOLMES_BSON_STRING

#include <string>

#include "holmes/bson/value.h"

namespace holmes::bson {

/** A BSON class to represent a UTF-8 string. */
class string:
	public value {
private:
	/** The string value. */
	std::string _value;
public:
	/** Construct BSON value containing UTF-8 string.
	 * @param value the required string value
	 */
	explicit string(const std::string& value);

	virtual std::unique_ptr<value> clone() const;
	virtual unsigned char type() const;
	virtual size_t length() const;
	virtual void _encode(writer& bw) const;
	virtual std::string to_json() const;

	/** Decode instance from octet string.
	 * @param bd the BSON data to be decoded
	 */
	static std::unique_ptr<string> decode(octet::string& bd);
};

} /* namespace holmes::bson */

#endif
