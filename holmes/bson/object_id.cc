// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>
#include <sstream>

#include "holmes/bson/writer.h"
#include "holmes/bson/object_id.h"

namespace holmes::bson {

object_id::object_id(const octet::string& value):
	_value(value) {

	if (_value.length() != 12) {
		throw std::invalid_argument("invalid length for BSON object ID");
	}
}

std::unique_ptr<value> object_id::clone() const {
	return std::make_unique<object_id>(*this);
}

unsigned char object_id::type() const {
	return 0x07;
}

size_t object_id::length() const {
	return _value.length();
}

void object_id::_encode(writer& bw) const {
	bw.write_bytes(_value.data(), _value.length());
}

std::string object_id::to_json() const {
	std::ostringstream hexout;
	hexout << _value;

	std::string result;
	result.append("{\"$oid\":\"");
	result.append(hexout.str());
	result.append("\"}");
	return result;
}

std::unique_ptr<object_id> object_id::decode(octet::string& bd) {
	octet::string value = read(bd, 12);

	return std::make_unique<object_id>(value);
}

} /* namespace holmes::bson */
