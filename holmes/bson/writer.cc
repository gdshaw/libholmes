// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cstring>
#include <stdexcept>

#include "holmes/bson/writer.h"

namespace holmes::bson {

void writer::write_byte(unsigned char value) {
	if (_remaining < 1) {
		throw std::out_of_range("BSON buffer full");
	}
	*_ptr++ = value;
	_remaining -= 1;
}

void writer::write_bytes(const void* data, size_t count) {
	if (_remaining < count) {
		throw std::out_of_range("BSON buffer full");
	}
	memcpy(_ptr, data, count);
	_ptr += count;
	_remaining -= count;
}

void writer::write_int32(int32_t value) {
	write_byte(value >> 0);
	write_byte(value >> 8);
	write_byte(value >> 16);
	write_byte(value >> 24);
}

void writer::write_int64(int64_t value) {
	write_byte(value >> 0);
	write_byte(value >> 8);
	write_byte(value >> 16);
	write_byte(value >> 24);
	write_byte(value >> 32);
	write_byte(value >> 40);
	write_byte(value >> 48);
	write_byte(value >> 56);
}

void writer::write_cstring(const char* value) {
	write_bytes(value, strlen(value));
	write_byte(0);
}

} /* namespace holmes::bson */
