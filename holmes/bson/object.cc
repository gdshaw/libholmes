// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>
#include <utility>
#include <algorithm>

#include "holmes/bson/writer.h"
#include "holmes/bson/null.h"
#include "holmes/bson/string.h"
#include "holmes/bson/object.h"
#include "holmes/bson/any.h"

namespace holmes::bson {

std::unique_ptr<value> object::clone() const {
	return std::make_unique<object>(*this);
}

unsigned char object::type() const {
	return 0x03;
}

size_t object::length() const {
	size_t len = 5;
	for (auto& member : _members) {
		len += 1 + member.first.length() + 1 + member.second.length();
	}
	return len;
}

void object::_encode(writer& bw) const {
	bw.write_int32(length());
	for (auto& member : _members) {
		bw.write_byte(member.second.type());
		bw.write_cstring(member.first.c_str());
		member.second._encode(bw);
	}
	bw.write_byte(0);
}

std::string object::to_json() const {
	std::string result;
	result.push_back('{');
	bool first = true;
	for (auto& member : _members) {
		if (first) {
			first = false;
		} else {
			result.push_back(',');
		}
		result.append(bson::string(member.first).to_json());
		result.push_back(':');
		result.append(member.second.to_json());
	}
	result.push_back('}');
	return result;
}

object::mapped_type& object::operator[](const std::string& name) {
	auto f = std::find_if(_members.begin(), _members.end(),
		[&name](const value_type& member) {
			return member.first == name;
		});
	if (f == _members.end()) {
		_members.push_back(std::make_pair(name, bson::any()));
		f = _members.end() - 1;
	}
	return f->second;
}

object::mapped_type& object::at(const std::string& name) {
	auto f = std::find_if(_members.begin(), _members.end(),
		[&name](const value_type& member) {
			return member.first == name;
		});
	if (f == _members.end()) {
		throw std::out_of_range("out of range");
	}
	return f->second;
}

const object::mapped_type& object::at(const std::string& name) const {
	auto f = std::find_if(_members.begin(), _members.end(),
		[&name](const value_type& member) {
			return member.first == name;
		});
	if (f == _members.end()) {
		throw std::out_of_range("out of range");
	}
	return f->second;
}

void object::insert(const std::string& name, const value& value) {
	(*this)[name] = value;
}

void object::insert(const std::string& name, const std::string& value) {
	insert(name, bson::string(value));
}

std::unique_ptr<object> object::decode(octet::string& bd) {
	std::unique_ptr<object> result = std::make_unique<object>();
	int32_t length = read_int32(bd, -1);
	if (length < 5) {
		throw std::invalid_argument("invalid length in BSON object");
	}
	octet::string content = read(bd, length - 4);

	unsigned char type = read_uint8(content);
	while (type != 0) {
		std::string k = read_cstring(content);
		std::unique_ptr<value> v = value::decode(type, content);
		result->insert(k, *v);
		type = read_uint8(content);
	}
	return result;
}

} /* namespace holmes::bson */
