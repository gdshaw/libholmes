// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cstdio>
#include <stdexcept>

#include "holmes/bson/writer.h"
#include "holmes/bson/string.h"

namespace holmes::bson {

string::string(const std::string& value):
	_value(value) {}

std::unique_ptr<value> string::clone() const {
	return std::make_unique<string>(*this);
}

unsigned char string::type() const {
	return 0x02;
}

size_t string::length() const {
	return _value.length() + 5;
}

void string::_encode(writer& bw) const {
	bw.write_int32(_value.length() + 1);
	bw.write_bytes(_value.data(), _value.length());
	bw.write_byte(0);
}

std::string string::to_json() const {
	// Characters are escaped only if they must be.
	// The reserved capacity currently makes no allowance for escaped
	// characters, however scanning would be necessary to obtain an
	// accurate predication in all cases, and for typical workloads
	// there would be a risk of this doing more harm than good.
	std::string result;
	result.reserve(_value.length() + 2);
	result.push_back('"');
	for (char c : _value) {
		if (c < 0x20) {
			switch (c) {
			case '\b':
				result.push_back('\\');
				result.push_back('b');
				break;
			case '\t':
				result.push_back('\\');
				result.push_back('t');
				break;
			case '\n':
				result.push_back('\\');
				result.push_back('n');
				break;
			case '\f':
				result.push_back('\\');
				result.push_back('f');
				break;
			case '\r':
				result.push_back('\\');
				result.push_back('r');
				break;
			default:
				char buffer[7];
				sprintf(buffer, "\\u%04X", c);
				result.append(buffer);
				break;
			}
		} else {
			switch (c) {
			case '"':
				result.push_back('\\');
				result.push_back('\"');
				break;
			case '\\':
				result.push_back('\\');
				result.push_back('\\');
				break;
			default:
				result.push_back(c);
				break;
			}
		}
	}
	result.push_back('"');
	return result;
}

std::unique_ptr<string> string::decode(octet::string& bd) {
	int32_t length = read_int32(bd, -1);
	if (length < 1) {
		throw std::invalid_argument("invalid length in BSON string");
	}
	bd.at(length - 1);
	octet::string content = read(bd, length);
	if (content[length - 1] != 0) {
		throw std::invalid_argument("missing terminator in BSON string");
	}
	std::string value(reinterpret_cast<const char*>(content.data()),
		length - 1);
	return std::make_unique<string>(value);
}

} /* namespace holmes::bson */
