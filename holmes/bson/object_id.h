// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_BSON_OBJECT_ID
#define HOLMES_BSON_OBJECT_ID

#include <string>

#include "holmes/octet/string.h"
#include "holmes/bson/value.h"

namespace holmes::bson {

/** A BSON class to represent an object ID. */
class object_id:
	public value {
private:
	/** The binary value. */
	octet::string _value;
public:
	/** Construct BSON value containing object ID.
	 * @param value the required binary value
	 */
	object_id(const octet::string& value);

	virtual std::unique_ptr<value> clone() const;
	virtual unsigned char type() const;
	virtual size_t length() const;
	virtual void _encode(writer& bw) const;
	virtual std::string to_json() const;

	/** Decode instance from octet string.
	 * @param bd the BSON data to be decoded
	 */
	static std::unique_ptr<object_id> decode(octet::string& bd);
};

} /* namespace holmes::bson */

#endif
