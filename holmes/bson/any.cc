// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "holmes/bson/null.h"
#include "holmes/bson/any.h"

namespace holmes::bson {

bson::value* any::_null = new bson::null;

std::unique_ptr<value> any::clone() const {
	return _ptr->clone();
}

unsigned char any::type() const {
	return _ptr->type();
}

size_t any::length() const {
	return _ptr->length();
}

bool any::is_null() const {
	return _ptr->is_null();
}

void any::_encode(writer& bw) const {
	return _ptr->_encode(bw);
}

std::string any::to_json() const {
	return _ptr->to_json();
}

any& any::at(const std::string& key) {
	return _ptr->at(key);
}

const any& any::at(const std::string& key) const {
	return _ptr->at(key);
}

any& any::operator[](const std::string& key) {
	return _ptr->operator[](key);
}

any& any::at(size_t index) {
	return _ptr->at(index);
}

const any& any::at(size_t index) const {
	return _ptr->at(index);
}

} /* namespace holmes::bson */
