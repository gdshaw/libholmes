// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include "holmes/bson/writer.h"
#include "holmes/bson/value.h"
#include "holmes/bson/string.h"
#include "holmes/bson/object.h"
#include "holmes/bson/array.h"
#include "holmes/bson/binary.h"
#include "holmes/bson/object_id.h"
#include "holmes/bson/boolean.h"
#include "holmes/bson/null.h"
#include "holmes/bson/int32.h"
#include "holmes/bson/int64.h"
#include "holmes/bson/any.h"

namespace holmes::bson {

bool value::is_null() const {
	return false;
}

any& value::at(const std::string& key) {
	throw std::runtime_error("BSON value does not support indexing by string");
}

const any& value::at(const std::string& key) const {
	throw std::runtime_error("BSON value does not support indexing by string");
}

any& value::operator[](const std::string& key) {
	throw std::runtime_error("BSON value does not support indexing by string");
}

any& value::at(size_t index) {
	throw std::runtime_error("BSON value does not support indexing by integer");
}

const any& value::at(size_t index) const {
	throw std::runtime_error("BSON value does not support indexing by integer");
}

octet::string value::encode() const {
	size_t len = length();

	auto buf = octet::buffer::make(len);
	writer bw(buf->data(), len);
	_encode(bw);
	return octet::string(*buf, buf->data(), len);
}

std::unique_ptr<value> value::decode(unsigned char type, octet::string& bd) {
	switch (type) {
	case 0x02:
		return bson::string::decode(bd);
	case 0x03:
		return bson::object::decode(bd);
	case 0x04:
		return bson::array::decode(bd);
	case 0x05:
		return bson::binary::decode(bd);
	case 0x07:
		return bson::object_id::decode(bd);
	case 0x08:
		return bson::boolean::decode(bd);
	case 0x0a:
		return std::make_unique<null>();
	case 0x10:
		return bson::int32::decode(bd);
	case 0x12:
		return bson::int64::decode(bd);
	default:
		throw std::invalid_argument("unsupported BSON type code");
	}
}

} /* namespace holmes::bson */
