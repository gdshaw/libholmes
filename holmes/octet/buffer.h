// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HOLMES_OCTET_BUFFER
#define HOLMES_OCTET_BUFFER

#include <cstdlib>

namespace holmes::octet {

/** A class for holding the shared content of an octet string. */
class buffer {
private:
	/** The number of references to this buffer.
	 * The buffer should be deleted if the reference count reaches zero.
	 */
	unsigned int _refcount = 1;

	/** A pointer to the buffer content. */
	unsigned char* _data;

	/** Construct octet buffer with co-allocated content.
	 * This constructor is intended for use by buffer::make.
	 * Memory for the buffer content is presumed to have been allocated
	 * immediately following the buffer object. It is private because
	 * it uses the size of this class as an offset, so is not suitable
	 * for use by subclasses.
	 */
	buffer():
		_data(reinterpret_cast<unsigned char*>(this) + sizeof(*this)) {}
public:
	/** Construct buffer object with separate content.
	 * @param data a pointer to the buffer content
	 */
	explicit buffer(unsigned char* data):
		_data(data) {}

	/** Destroy octet buffer. */
	virtual ~buffer() = default;

	/** Get buffer content.
	 * @return a pointer to the buffer content.
	 */
	unsigned char* data() {
		return _data;
	}

	/** Allocate buffer on heap.
	 * @param header_size the size of the header object
	 * @param capacity the required buffer capacity, in octets
	 */
	void* operator new(size_t header_size, size_t capacity = 0) {
		return malloc(header_size + capacity);
	}

	/** Deallocate buffer on heap.
	 * This will have the effect of deallocating both the header object and
	 * any co-allocated data.
	 * @param buf the buffer object to deallocate
	 */
	void operator delete(void* buf) {
		free(buf);
	}

	/** Make buffer object on heap with co-allocated content.
	 * A single heap allocation is used to provide for both the
	 * buffer object itself, and the buffer content.
	 * @param capacity the required capacity, in octets
	 * @return the resulting buffer object
	 */
	static buffer* make(size_t capacity) {
		return new(capacity) buffer;
	}

	/** Make new link to buffer object.
	 * This function causes the reference count to be incremented.
	 * @param buf the buffer to be linked
	 * @return a pointer to the linked buffer
	 */
	static buffer* link(buffer& buf) {
		++buf._refcount;
		return &buf;
	}

	/** Release link to buffer object.
	 * This function causes the reference count to be decremented,
	 * and the buffer deleted if the count reaches zero.
	 * @param buf the buffer to be unlinked
	 */
	static void unlink(buffer& buf) {
		if (!--buf._refcount) {
			delete &buf;
		}
	}
};

} /* namespace holmes::octet */

#endif
