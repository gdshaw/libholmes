// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include "holmes/libc_error.h"
#include "holmes/octet/file_buffer.h"

namespace holmes::octet {

file_buffer::file_buffer(const std::string& pathname):
	buffer(0) {

	// Open the file.
	int fd = open(pathname.c_str(), O_RDONLY);
	if (fd == -1) {
		throw libc_error();
	}

	// Determine the length of the file.
	struct stat sb;
	if (fstat(fd, &sb) == -1) {
		close(fd);
		throw libc_error();
	}
	_length = sb.st_size;

	// Map the content of the file into memory.
	void* data = mmap(0, _length, PROT_READ, MAP_PRIVATE, fd, 0);
	if (data == MAP_FAILED) {
		close(fd);
		throw libc_error();
	}

	// Initialise the buffer.
	buffer::operator=(buffer(static_cast<unsigned char*>(data)));

	// Close the file.
	close(fd);
}

file_buffer::~file_buffer() {
	munmap(data(), _length);
}

} /* namespace holmes::octer */
