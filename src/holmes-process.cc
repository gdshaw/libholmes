// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cstdlib>
#include <iostream>

#include <getopt.h>

#include "holmes/bson/null.h"
#include "holmes/bson/int32.h"
#include "holmes/bson/string.h"
#include "holmes/bson/binary.h"
#include "holmes/db/database.h"
#include "holmes/net/ethernet/frame.h"
#include "holmes/net/inet4/datagram.h"
#include "holmes/net/udp/datagram.h"
#include "holmes/net/tcp/segment.h"

using namespace holmes;
using namespace holmes::net;

static const int decode_version = 0;

void write_help(std::ostream& out) {
	out << "Usage: holmes-process <database>" << std::endl;
}

/** Make connection tracking key.
 * This is an octet string containing the 5-tuple used to distinguish
 * concurrent connections, but with the source and destination socket
 * addresses sorted into an order based on their content. This has the effect
 * of mapping packets travelling in the forward and reverse direction onto
 * the same key if they refer to the same unordered pair of socket addresses
 * (and therefore, potentially, belong to the same connection).
 * @param protocol the IP protocol number
 * @param src_addr the source address
 * @param src_port the source port number
 * @param dst_addr the destination address
 * @param dst_port the destination port number
 * @return the connection tracking key
 */
octet::string make_ctkey(uint8_t protocol,
	const inet::address& src_addr, uint16_t src_port,
	const inet::address& dst_addr, uint16_t dst_port) {

	std::basic_string<unsigned char> src_sockaddr(
		src_addr.data().data(), src_addr.data().length());
	src_sockaddr.push_back(src_port >> 8);
	src_sockaddr.push_back(src_port >> 0);

	std::basic_string<unsigned char> dst_sockaddr(
		dst_addr.data().data(), src_addr.data().length());
	dst_sockaddr.push_back(dst_port >> 8);
	dst_sockaddr.push_back(dst_port >> 0);

	std::basic_string<unsigned char> ctkey;
	ctkey.push_back(protocol);
	if (src_sockaddr < dst_sockaddr) {
		ctkey.append(src_sockaddr);
		ctkey.append(dst_sockaddr);
	} else {
		ctkey.append(dst_sockaddr);
		ctkey.append(src_sockaddr);
	}
	return octet::string(ctkey);
}

void process_tcp(bson::object& out, const inet::datagram& inet_dgram,
	octet::string raw) {

	tcp::segment seg(inet_dgram, raw);
	out.insert("tcp", seg.to_bson());

	octet::string ctkey = make_ctkey(6,
		inet_dgram.src_addr(), seg.src_port(),
		inet_dgram.dst_addr(), seg.dst_port());
	out.insert("ctkey", bson::binary(ctkey));
}

void process_udp(bson::object& out, const inet::datagram& inet_dgram,
	octet::string raw) {

	udp::datagram udp_dgram(inet_dgram, raw);
	out.insert("udp", udp_dgram.to_bson());

	octet::string ctkey = make_ctkey(17,
		inet_dgram.src_addr(), udp_dgram.src_port(),
		inet_dgram.dst_addr(), udp_dgram.dst_port());
	out.insert("ctkey", bson::binary(ctkey));
}

void process_inet4(bson::object& out, octet::string raw) {
	inet4::datagram dgram(raw);
	out.insert("inet4", dgram.to_bson());
	switch (dgram.protocol()) {
	case 6:
		process_tcp(out, dgram, dgram.payload());
		break;
	case 17:
		process_udp(out, dgram, dgram.payload());
		break;
	}
}

void process_ethernet(bson::object& out, octet::string raw) {
	ethernet::frame frame(raw);
	out.insert("ethernet", frame.to_bson());
	switch (frame.ethertype()) {
	case 0x0800:
		process_inet4(out, frame.payload());
		break;
	}
}

int main(int argc, char* argv[]) {
	unsigned int verbosity = 0;

	int opt;
	while ((opt = getopt(argc, argv, "v")) != -1) {
		switch (opt) {
		case 'v':
			verbosity += 1;
			break;
		}
	}

	if (optind + 1 != argc) {
		write_help(std::cerr);
		exit(1);
	}
	std::string uri_string = argv[optind++];

	try {
		auto db = db::database::make(uri_string);
		auto coll = db->collection("packets");
		bson::object filter;
		filter.insert("state", bson::null());
		auto unprocessed = coll->find(filter, 1000);

		for (auto member : unprocessed) {
			auto& before = member.as<bson::object>();
			if (verbosity > 0) {
				std::cout << before.at("_id").to_json() << std::endl;
			}
			bson::object after = before;
			octet::string raw = before.at("packet").as<bson::binary>();
			process_ethernet(after, raw);
			after.insert("state", bson::int32(1));
			coll->update(before, after);
		}
	} catch (std::exception& ex) {
		std::cerr << ex.what() << std::endl;
		exit(1);
	}
	return 0;
}
