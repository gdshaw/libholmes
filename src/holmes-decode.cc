// This file is part of libholmes.
// Copyright 2020 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cstdlib>
#include <iostream>

#include <getopt.h>

#include "holmes/octet/string.h"
#include "holmes/octet/file.h"
#include "holmes/pcap/file.h"
#include "holmes/net/ethernet/frame.h"
#include "holmes/net/inet4/datagram.h"
#include "holmes/net/inet6/datagram.h"
#include "holmes/net/tcp/segment.h"
#include "holmes/net/udp/datagram.h"
#include "holmes/net/dns/message.h"

using namespace holmes;

void write_help(std::ostream& out) {
	out << "Usage: holmes-decode <pathname>" << std::endl;
}

bson::object decode(pcap::record rec) {
	bson::object result;
	holmes::net::ethernet::frame frame(rec.payload());
	result.insert("ethernet", frame.to_bson());
	auto ether_payload = frame.payload();

	if (frame.ethertype() == 0x0800) {
		holmes::net::inet4::datagram inet_dgram(ether_payload);
		result.insert("inet4", inet_dgram.to_bson());
		auto inet_payload = inet_dgram.payload();
		switch (inet_dgram.protocol()) {
		case 6: {
			holmes::net::tcp::segment tcp_seg(inet_dgram, inet_payload);
			result.insert("tcp", tcp_seg.to_bson());
			break; }
		case 17: {
			holmes::net::udp::datagram udp_dgram(inet_dgram, inet_payload);
			result.insert("udp", udp_dgram.to_bson());
			if ((udp_dgram.src_port() == 53) || (udp_dgram.dst_port() == 53)) {
				holmes::net::dns::message dns_msg(udp_dgram.payload());
				result.insert("dns", dns_msg.to_bson());
			}
			break; }
		}
	} else if (frame.ethertype() == 0x086dd) {
		holmes::net::inet6::datagram inet_dgram(ether_payload);
		result.insert("inet6", inet_dgram.to_bson());
		auto inet_payload = inet_dgram.payload();
		switch (inet_dgram.next_header()) {
		case 6: {
			holmes::net::tcp::segment tcp_seg(inet_dgram, inet_payload);
			result.insert("tcp", tcp_seg.to_bson());
			break; }
		case 17: {
			holmes::net::udp::datagram udp_dgram(inet_dgram, inet_payload);
			result.insert("udp", udp_dgram.to_bson());
			if ((udp_dgram.src_port() == 53) || (udp_dgram.dst_port() == 53)) {
				holmes::net::dns::message dns_msg(udp_dgram.payload());
				result.insert("dns", dns_msg.to_bson());
			}
			break; }
		}
	}
	return result;
}

void decode(const std::string& pathname, bool join) {
	if (join) {
		std::cout << '[';
	}

	bool first = true;
	try {
		octet::file file(pathname);
		pcap::file pf(file);

		while (true) {
			bson::object result = decode(pf.read());
			if (join) {
				if (first) {
					first = false;
				} else {
					std::cout << ',';
				}
			}
			std::cout << result.to_json();
			if (!join) {
				std::cout << '\n';
			}
		}
	} catch (std::out_of_range&) {
		/** No action. */
	}

	if (join) {
		std::cout << ']';
	}
}

int main(int argc, char* argv[]) {
	bool join = false;

	int opt;
	while ((opt = getopt(argc, argv, "j")) != -1) {
		switch (opt) {
		case 'j':
			join = true;
			break;
		}
	}

	if (optind == argc) {
		std::cerr << "PCAP file pathname not specified" << std::endl;
		exit(1);
	}
	std::string pathname = argv[optind++];

	try {
		decode(pathname, join);
	} catch (std::exception& ex) {
		std::cerr << ex.what() << std::endl;
		exit(1);
	}
	return 0;
}
